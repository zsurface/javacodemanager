
### Code Manager in Java ###
* Read snippet file and parse the header of each block of text 
* Build prefix strings map for all the headers 
* Use the prefix strings for fast search 
* JavaFX is used for UI
- Sun Sep  9 18:35:30 2018 
* Change TextArea to ListView
* CTRL-C to copy selected cells from ListView
* Change color of "---"
* Pattern match on specific cell and change the cell color, e.g. background color
- It seems to me the color of text can not be changed on the same cell individually  
*
* Mon Sep 10 00:11:27 2018 
* Disable some TextField focus so that Tabs can be ignored
* Add more css stuff to ListView
* Add repeat function to Aron.java, it is simple but very useful:)
* -------------------------------------------------------------------------------- 
* Mon Sep 10 12:48:50 2018 
* Add F3 to icontified the window
* Add transparent to window
* Add F2 to half the window size and move to right of screen

### change logs ###
* Sun Sep  9 09:29:21 2018 
* Auto resize width of TextArea
* Add interface class and implementation for Haskell and Vim snippet
 

### TODO ###
* Add css skin for UI 
* Add parse for Java code.

### dependency ###
*  depend on:  
* /Users/cat/myfile/bitbucket/javalib/jar
* /Users/cat/myfile/bitbucket/javalib/classfile
### Change libraries.xml and jar.xml in .idea
* Change it to absolute path to classfile and jar
* It was relative path to current project, there is problem when copy the project to other dir


### screenshot ###
![image](src/JavaCodeManager.png = 200x200)
* New screenshot, Mon Sep 10 12:45:11 2018 
![image](src/JavaCode.png = 200x200)
