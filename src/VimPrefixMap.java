import classfile.Aron;

import java.util.*;


/**
 * Created by cat on 9/8/18.
 *
 * Fri May  3 17:31:37 PDT 2019
 * fixed  prefix => prefixStr
 * fixed method name typo error
 * Use javalib8 for classfile and jar files:
 */
public class VimPrefixMap implements PrefixMap{
    String pattern = "::";
    public VimPrefixMap(String pattern){
        this.pattern = pattern;
    }

    public List<String> allLines(List<String> blockText){
        List<String> allTokens = new ArrayList<>();
        for(String s: blockText){
            List<String> tokens = keyList(Aron.split(s.trim(), pattern));
            allTokens = Aron.concatList(allTokens, tokens);
        }
        return allTokens;
    }
    public Map<String, List<List<String>>> buildPrefixMap(List<List<String>> lists){
        Map<String, List<List<String>>> map = new HashMap<>();
        for(List<String> blockText : lists){
            if(blockText.size() > 0){

                //
                // latex_file :*.txt: cat dog pig, cow => [latex_file, *.txt, dog, cat]
                // List<String> tokens = Aron.split(blockText.get(0).trim(), "\\s*:\\s*|\\s*,\\s*|\\s+");
                // latex_file :*.txt: cat dog pig => [latex_file, *.txt, dog cat pig]
                //                   "cat dog pig"=> ["cat dog pig", "dog pig", "pig"]
                //List<String> tokens = keyList(Aron.split(blockText.get(0).trim(), pattern));
                List<String> tokens = allLines(blockText);


                Set<String> uniqueSet = new HashSet<String>();
                for (String token : tokens) {
                    String key = token.trim();

                    List<String> prefixList = Aron.prefixStr(key);
                    for(String preStr: prefixList){
                        if (!uniqueSet.contains(preStr)){
                            uniqueSet.add(preStr);

                            List<List<String>> listValue = map.get(preStr);

                            if (listValue != null) {
                                listValue.add(blockText);
                            } else {
                                List<List<String>> list2d = new ArrayList<>();
                                list2d.add(blockText);
                                map.put(preStr, list2d);
                            }
                        }
                    }
                }
            }
        }
        return map;
    }

    private static List<String> keyList(List<String> list){
        List<String> mlist = new ArrayList<String>();
        List<String> empty = new ArrayList<String>();
        if (list != null && list.size() > 2){
            List<String> first = list.subList(0, 1);
            List<String> last  = list.subList(2, list.size());
            mlist = Aron.mergeLists(first, last);
        }
        else{
            mlist = Aron.mergeLists(empty, list);
        }
        return mlist;
    }
}
