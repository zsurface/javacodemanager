import java.util.List;
import java.util.Map;

/**
 * Created by cat on 9/7/18.
 */
public interface PrefixMap {
    public Map<String, List<List<String>>> buildPrefixMap(List<List<String>> lists);
}
