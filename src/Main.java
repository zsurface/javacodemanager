import classfile.Aron;
import classfile.Print;
import classfile.Test;
import classfile.StopWatch;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
* Thu Sep  6 16:47:36 2018 
* Change snippet file path from local var to parameter in processCodeFile
* -------------------------------------------------------------------------------- 
* Mon Sep 10 12:41:10 2018 
* Add transparent
* F3 Key to icontified the window
* F2 Half the window and move the window to right size
*/
public class Main  extends Application {
    StopWatch stopWatch = new StopWatch();
    StopWatch keyPressStopWatch = new StopWatch();

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(final Stage primaryStage) throws Exception{
        //primaryStage.setOpacity(0.8);
        stopWatch.start();
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        final String patternSnippet  = "\\s*:\\s*|\\s*,\\s*";
        //final String patternSnippet  = "[:, ]";
        final String fNameSnippet = "/Users/cat/myfile/bitbucket/snippets/snippet.hs";
        // final String fName = "/Users/cat/try/tryaron.hs";

        final Map<String, List<List<String>>> codeMapSnippet = processVimCodeFile(fNameSnippet, patternSnippet);
        Print.p("codeMapSnippet size=" + codeMapSnippet.size());

        final String patternHaskell  = "::";
        final String fNameHasekell = "/Users/cat/myfile/bitbucket/haskell/AronModule.hs";

        final Map<String, List<List<String>>> codeMapHaskell = processHaskellCodeFile(fNameHasekell, patternHaskell);
        Print.p("codeMapHaskell size=" + codeMapHaskell.size());

        final Map<String, List<List<String>>> codeMap = Aron.mergeMapListList(codeMapSnippet, codeMapHaskell);

        final Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Error");

        final ListView<String> listView = new ListView<>();
        listView.setEditable(true);

        listView.setMinHeight(primaryScreenBounds.getHeight() - 100);
        final ClipboardContent clipboardContent = new ClipboardContent();

        final KeyCombination keyCombinationShiftC = new KeyCodeCombination(
                KeyCode.C, KeyCombination.CONTROL_DOWN);


        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setCellFactory(TextFieldListCell.forListView());
        listView.setOnEditCommit(new EventHandler<ListView.EditEvent<String>>() {
            @Override
            public void handle(ListView.EditEvent<String> t) {
                listView.getItems().set(t.getIndex(), t.getNewValue());

                ObservableList<String> selectedItems =  listView.getSelectionModel().getSelectedItems();
                for(String s : selectedItems){
                    System.out.println("selected item " + s);
                }
            }
        });

        final StringBuilder stringBuilder = new StringBuilder();

        listView.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<String>() {
                    public void changed(
                            ObservableValue<? extends String> observable,
                            String oldValue, String newValue) {
                        clipboardContent.putString(stringBuilder.toString());

                        stringBuilder.setLength(0);
                        ObservableList<String> selectedItems =  listView.getSelectionModel().getSelectedItems();
                        for(String s : selectedItems){
                            System.out.println("KeyBoard selected " + s);
                            stringBuilder.append(s);
                        }
                    }
                });

        listView.setOnMouseClicked(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                ObservableList<String> selectedItems =  listView.getSelectionModel().getSelectedItems();

                for(String s : selectedItems){
                    System.out.println("Mouse selected " + s);
                }
                Print.pbl("CTRL + C Pressed");
                Clipboard.getSystemClipboard().clear();
                clipboardContent.putString(stringBuilder.toString());
                Clipboard.getSystemClipboard().setContent(clipboardContent);
                stringBuilder.setLength(0);

            }

        });

        listView.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (keyCombinationShiftC.match(event)) {

                    Print.pbl("CTRL + C Pressed");
                    Clipboard.getSystemClipboard().clear();
                    clipboardContent.putString(stringBuilder.toString());
                    Clipboard.getSystemClipboard().setContent(clipboardContent);
                    stringBuilder.setLength(0);
                }
            }
        });

        listView.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                            //setStyle("-fx-control-inner-background: " + DEFAULT_CONTROL_INNER_BACKGROUND + ";");
                        } else {
                            setText(item);

                            Pattern pattern = Pattern.compile("\\s*String\\s*");
                            Pattern headerPattern = Pattern.compile("\\s*:[^:]+:\\s*");
                            Matcher matcher = pattern.matcher(item);
                            Matcher headerMatch = headerPattern.matcher(item);

                            if (item.startsWith("---")) {
                                setText(item);
                                setTextFill(Color.RED);
                                //setStyle("-fx-control-inner-background: " + HIGHLIGHTED_CONTROL_INNER_BACKGROUND + ";");
                            } else if (matcher.find()) {
                                setText(item);
                                setTextFill(Color.BLACK);

                            } else if (headerMatch.find()){
                                setText(item);
                                setTextFill(Color.GRAY);
                            } else {
                                setText(item);
                                setTextFill(Color.BLACK);
                            }
                        }
                    }
                };
            }
        });

        final TextArea textAreaFile = new TextArea();
        textAreaFile.setMinHeight(750);

        Label prefixLable = new Label("Total:");
        final Label matchNumLabel = new Label("Match #:");

        final TextField prefixTF = new TextField ();
        final TextField matchNumTF = new TextField ();
        prefixTF.setFocusTraversable(false);
        matchNumTF.setFocusTraversable(false);

        final TextField searchTF = new TextField ();
        //searchTF.setFocusTraversable(false);

        final HBox searchBox = new HBox();
        searchBox.getChildren().add(searchTF);
        //searchBox.getChildren().addAll(matchNumLabel, matchNumTF);

        HBox hBoxSearch  = new HBox();

        hBoxSearch.setAlignment(Pos.CENTER);
        hBoxSearch.setPadding(new Insets(1, 1, 1, 1));
        hBoxSearch.setSpacing(2);
        hBoxSearch.getChildren().addAll(searchTF);

        //hBoxSearch.getChildren.addAll(prefixLable, prefixTF);

        VBox vboxTextArea = new VBox();
        vboxTextArea.setAlignment(Pos.CENTER);
        vboxTextArea.autosize();
        vboxTextArea.setPadding(new Insets(1, 1, 1, 1));

        // ref https://stackoverflow.com/questions/31013329/javafx-resizing-textfield-with-window
        // automatically resize textArea JavaFX
        //HBox.setHgrow(textAreaFile, Priority.ALWAYS);
        //vboxTextArea.getChildren().addAll(textAreaFile);
        vboxTextArea.getChildren().add(listView);

        VBox vBox = new VBox();
        vBox.setStyle("-fx-border-color : black; -fx-border-width : 1 ");
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.setSpacing(1);
        vBox.getChildren().addAll(hBoxSearch);
        vBox.getChildren().addAll(vboxTextArea);

        /**
        * Event for key pressed on search TextField
        */
        searchTF.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent key) {
                Print.pbl("Key Pressed: " + key.getText());
            }
        });

        /**
        * display all the code in textArea  when keyboard releases
         * Get input from user
        * codeMap has all the prefix string snippets.hs file
        */
        searchTF.setOnKeyReleased(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent key) {

                keyPressStopWatch.start();
                if (key.getCode() == KeyCode.F1) {
                    primaryStage.setMaximized(true);
                } else if(key.getCode() == KeyCode.F2) {
                    primaryStage.setX(primaryScreenBounds.getWidth() / 2);
                    primaryStage.setWidth(primaryScreenBounds.getWidth() / 2);
                    primaryStage.setHeight(primaryScreenBounds.getHeight());
                } else if (key.getCode() == KeyCode.F3){
                    primaryStage.setIconified(true);
                } else {

                    // get input from user
                    Print.pbl("Key Released: " + key.getText());
                    keyPressStopWatch.printTime();

                    String input = searchTF.getText();
                    List<List<String>> lists = codeMap.get(input);
                    listView.getItems().clear();
                    if (lists != null) {
                        for (List<String> list : lists) {
                            TextArea textArea = new TextArea();

//                            for (String s : list) {
//                                //textAreaFile.appendText(s);
//                                //textArea.appendText(s);
//                                listView.getItems().add(s);
//                            }
                            listView.getItems().addAll(list);
                            listView.getItems().addAll(Aron.repeat(120, "-"));
                            //textAreaFile.appendText(Aron.repeat(120, "-") + "\n");
                        }
                        matchNumTF.clear();
                        matchNumTF.insertText(0, lists.size() + "");
                    }
                }

            }
        });



//        Timeline timeline = new Timeline(new KeyFrame(
//                Duration.millis(2500),
//                e -> Print.pbl(e.toString())
//        ));
//        timeline.setCycleCount(Animation.INDEFINITE);
//        timeline.play();

        HBox hBoxPrefixCount = new HBox();
        HBox hBoxMatchNumber = new HBox();

        prefixTF.insertText(0, codeMap.size() + "");
        hBoxPrefixCount.getChildren().addAll(prefixLable, prefixTF);
        hBoxPrefixCount.setSpacing(2);
        hBoxPrefixCount.setPadding(new Insets(1, 1, 1, 1));
        hBoxPrefixCount.setStyle("-fx-background-color: #EEEAAA;");

        hBoxMatchNumber.getChildren().addAll(matchNumLabel, matchNumTF);
        hBoxMatchNumber.setSpacing(2);
        hBoxMatchNumber.setPadding(new Insets(1, 1, 1, 1));
        hBoxMatchNumber.setStyle("-fx-background-color: #EEE111;");

        HBox hBoxBottom = new HBox();
        hBoxBottom.setStyle("-fx-border-color : black; -fx-border-width : 1 ");

        hBoxBottom.setAlignment(Pos.CENTER);
        hBoxBottom.getChildren().addAll(hBoxPrefixCount);
        hBoxBottom.getChildren().addAll(hBoxMatchNumber);

        vBox.setStyle("-fx-border-color : black; -fx-border-width : 1 ");
        vBox.getChildren().addAll(hBoxBottom);

        Scene scene = new Scene(vBox, primaryScreenBounds.getWidth()/2, primaryScreenBounds.getHeight());
        scene.getStylesheets().add("style.css");

        scene.setFill(Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();


        stopWatch.printTime(); 
        Print.p("codeMapSnippet size=" + codeMapSnippet.size());
        Print.p("codeMapHaskell size=" + codeMapHaskell.size());

       scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent> () {
            @Override
            public void handle(KeyEvent t) {
              if(t.getCode()==KeyCode.ESCAPE) {
                  Print.pbl("press = " + t.getCode().toString());
                  primaryStage.close();
              }else if(t.getCode() == KeyCode.F1) {
                  Print.pbl("press = " + t.getCode().toString());
                  primaryStage.setMaximized(true);
              }else if(t.getCode() == KeyCode.SPACE){
                  Print.pbl( "press = " + t.getCode().toString());
              }else{
                  Print.pbl("keyCode=" + t.getCode().toString());
              }
            }
        });


       listView.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent> () {
            @Override
            public void handle(KeyEvent key) {
                if (key.getCode() == KeyCode.F1) {
                    primaryStage.setMaximized(true);
                } else if(key.getCode() == KeyCode.F2) {
                    primaryStage.setX(primaryScreenBounds.getWidth() / 2);
                    primaryStage.setWidth(primaryScreenBounds.getWidth() / 2);
                    primaryStage.setHeight(primaryScreenBounds.getHeight());
                } else if (key.getCode() == KeyCode.F3){
                    primaryStage.setIconified(true);
                } else if (key.getCode() == KeyCode.ESCAPE){
                    primaryStage.close();
                } else{
                  Print.pbl("listView keyCode=" + key.getCode().toString());
              }

            }
        });

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.F5) {
                    System.out.println("F5 pressed");
                    keyEvent.consume();
                }
            }
        });
    }
    
    public static Map<String, List<List<String>>> processVimCodeFile(String fName, String pattern) {
        List<List<String>> list2d = Aron.readFileSeparator(fName);

        VimPrefixMap prefixMap = new VimPrefixMap(pattern);
        Map<String, List<List<String>>> map = prefixMap.buildPrefixMap(list2d);
        return map;
    }

    public static Map<String, List<List<String>>> processHaskellCodeFile(String fName, String pattern) {
        List<List<String>> list2d = Aron.readFileSeparator(fName);
        HaskellPrefixMap prefixMap = new HaskellPrefixMap("::");
        Map<String, List<List<String>>> map = prefixMap.buildPrefixMap(list2d);
        return map;
    }
}
