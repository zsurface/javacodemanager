import classfile.Aron;

import java.util.*;

/**
 * Created by cat on 9/7/18.
 */
public class HaskellPrefixMap implements PrefixMap {
    String pattern = "::";
    public HaskellPrefixMap(String pattern){
        this.pattern = pattern;
    }


    public Map<String, List<List<String>>> buildPrefixMap(List<List<String>> lists){
        Map<String, List<List<String>>> map = new HashMap<>();
        for(List<String> blockText : lists){
            if(blockText.size() > 0){


                // fun::Int->Int => fun
                //
                // -- awesome function
                // fun::String->String => fun
                //
                // extract the function name only
                // remove " -- " line from haskell file so that function name can be matched
                // Note: final block of text will include comment line, " -- "
                //
                List<String> tokens_nocomment = Aron.filter("^\\s*--", blockText);
                if (tokens_nocomment.size() > 0) {
                    List<String> tokens = keyList(Aron.split(tokens_nocomment.get(0).trim(), pattern));

                    Set<String> uniqueSet = new HashSet<String>();
                    for (String token : tokens) {
                        String key = token.trim();

                        List<String> prefixList = Aron.prefixStr(key);

                        for (String preStr : prefixList) {
                            if (!uniqueSet.contains(preStr)) {
                                uniqueSet.add(preStr);

                                List<List<String>> listValue = map.get(preStr);

                                if (listValue != null) {
                                    listValue.add(blockText);
                                } else {
                                    List<List<String>> list2d = new ArrayList<>();
                                    list2d.add(blockText);
                                    map.put(preStr, list2d);
                                }
                            }
                        }
                    }
                }
            }
        }
        return map;
    }

    private static List<String> keyList(List<String> list){
        List<String> mlist = new ArrayList<String>();
        List<String> empty = new ArrayList<String>();
        if (list != null && list.size() > 2){
            List<String> first = list.subList(0, 1);
            List<String> last  = list.subList(2, list.size());
            mlist = Aron.mergeLists(first, last);
        }
        else{
            mlist = Aron.mergeLists(empty, list);
        }
        return mlist;
    }
}
